# MR-test-project

We want a list of students and their corresponding groups


We want it in the form of

```

## Group Xn

* John Doe
* Mary Something
* Extra Name

```

## Group A1 

* Mathias Knudsen
* Rudi Hansen
* Jakob Dahl
* Mikkel Henriksen
* Lauge Mathiesen

## Group A2 

* Nicolai Larsen
* Oscar Harttung
* Benjamin Bom Christensen
* Manisha Gurung
* Rune Mathias Petersen
*Julius Ingeli
* Greta Sumskaite

## Group A3 
* Bogdan Robert
* Mark Christiansen 
* Andrei Romar
* Jacob Berg Koch
* Kevin Herhold Larsen
* Can Gabriel Bekil
## Group A4
* Eimantas Dima
* Balázs Erdős
* Reuben Badham
* Allan Nielsen
* Mansuri Hassan 

## Group B1 
* Casper Nielsen
* Simon Bjerre
* Vladimir Rotaru
* Jonas Binti Jensen
* Hrvoje Clekovic

## Group B2 
* Nikolaj Hult-Christensen  
* Alexander Bjørk Andersen
* Aleksis Kairiss
* Jacob Suurballe Petersen
* Dainty Lyka Bihay Olsen
* Daniel Rasmussen
* Sheriff Dibba


## Group B3 
* Aubrey Jones
* Henrik Holm Hansen
* Aleksandra Voronina
* Camilla Errendal
* Lukas Zwak


## Group B4

* Ulrik Vinther-Jensen
* Mathias Andersen
* Bogdan-Alexandru Vanghelie
* Nikandras Kinderis
* Matas Krikstaponis

